import uio

from pigwatch.mainloop import *

__all__ = [ "BufferedFile" ]

class BufferedFile:
    def __init__(self, fn, mode="wb", block_size=512, max_age=120):
        self.block_size = block_size
        self.max_age = max_age

        self.fn = fn
        self._fp = open(fn, mode)
        self._reset()
        main().add_timeout(self.max_age, self._check_age)

    def _reset(self):
        self._in_buffer = 0
        self._buffer = uio.BytesIO()
        self._age_start = None

    def _flush(self):
        self._fp.write(self._buffer.getvalue())
        self._fp.flush()
        print("buffer flush")
        self._reset()

    def write(self, data):
        if isinstance(data, str):
            data = data.encode("utf-8")
        print("buffered write: %r" % data)
        self._buffer.write(data)
        self._in_buffer += len(data)
        if self._age_start is None:
            self._age_start = time.time()
        if self._in_buffer > self.block_size:
            print("%s block full flush" % self.fn)
            self._flush()
        else:
            self._check_age()

    def _check_age(self):
        if self._age_start is None: # no data
            return True
        age = time.time() - self._age_start
        if age > self.max_age:
            print("%s age flush after %.1fs" % (self.fn, age))
            self._flush()
        return True
