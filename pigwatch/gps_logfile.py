from .buffered_file import *
import ustruct

__all__ = [ "GPSLogfile" ]

class GPSLogfile:
    def __init__(self, fn, gps, max_age=60):
        self.buffered_file = BufferedFile(fn, "ab", max_age=max_age)
        self.gps = gps

    def record(self):
        lp = self.gps._last_pos
        if self.gps._have_fix:
            nsat = lp[4]
        else:
            nsat = 0
        if lp is not None:
            data = ustruct.pack("fffB", lp[1], lp[2], lp[3], nsat)
        else:
            data = ustruct.pack("fffB", 0, 0, 0, 0)
        self.buffered_file.write(
            self.gps.get_time().encode("utf-8") + data)
