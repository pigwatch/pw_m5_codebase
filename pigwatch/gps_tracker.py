import math

from m5stack import *
from m5ui import *
from uiflow import *
import unit

from pigwatch.mainloop import *

__all__ = [ "GPSTracker" ]

class GPSTracker:
    def __init__(self, utc_offset=2, rtc=None, poll_interval=5):
        self.gps = unit.get(unit.GPS, unit.PORTA)
        self.gps.set_time_zone(utc_offset)
        self.rtc = rtc

        self._motion_cbs = []
        self._have_fix = False
        self._last_pos = None
        # or (rtc_datetime, lat, lon, alt, n_sat)

        main().add_timeout(poll_interval, self._poll_gps)
        # todo: hardware does not know about this poll_interval!

    def register_on_motion(self, dist, cb):
        self._motion_cbs.append((dist, cb))

    def _check_rtc(self):
        if not self.rtc:
            return
        y, m, d, wd, H, M, S, _ = tt = self.rtc.datetime()
        ts = self.get_ts(tt)
        #print("rtc", self.get_time(tt))

        gH, gM, gS = map(int, self.gps.gps_time.split(":"))
        #print("self.gps.gps_date", self.gps.gps_date)
        gd, gm, gy = map(int, self.gps.gps_date.split("/"))
        gy += 2000
        gtt = (gy, gm, gd, wd, gH, gM, gS, 0)
        gts = self.get_ts(gtt)
        #print("gps", self.get_time(gtt))

        delta = gts - ts
        #print("ts delta: %.1f" % delta)
        if delta > 2 or delta < -2:
            self.rtc.datetime((gy, gm, gd, wd, gH, gM, gS, 0))
            print("did set rtc to gps: %s" % self.get_time())

    def get_ts(self, tt=None):
        """
        expects time-tuple
        """
        if tt is None:
            tt = self.rtc.datetime()
        y, m, d, wd, H, M, S, _ = tt
        days = (y - 2000) * 365.25 + m * 31.25 + d
        return days * 24 * 3600 + H * 3600 + M * 60 + S

    def get_time(self, tt=None):
        """
        returns iso date str YYYY-mm-dd HH:MM:SS
        from rtc
        expects time-tuple
        """
        if tt is None:
            tt = self.rtc.datetime()
        y, m, d, wd, H, M, S, _ = tt
        return "%d-%02d-%02d %02d:%02d:%02d" % (
            y, m, d, H, M, S)

    def _poll_gps(self):
        if self.gps.pos_quality == "1":
            self._check_rtc()
            new_pos = (
                self.rtc.datetime(),
                self.gps.latitude_decimal,
                self.gps.longitude_decimal,
                float(self.gps.altitude),
                int(self.gps.satellite_num)
            )
            had_fix = self._have_fix
            if self._motion_cbs:
                moved = self.surface_distance(new_pos)
            else:
                moved = 0
            #print("gps new %.4f,%.4f,%.0f,%d, moved %s" % (
            #    new_pos[1], new_pos[2], new_pos[3], new_pos[4], moved))
            self._last_pos = new_pos
            self._have_fix = True
            for dist, cb in self._motion_cbs:
                if not had_fix or moved is None or moved > dist:
                    cb()
        else:
            if self._have_fix:
                for dist, cb in self._motion_cbs:
                    cb() # notify about lost fix
            self._have_fix = False
        return True

    def surface_distance(self, a, b=None):
        """
        return est distance on earth surface from point a to b in meter
        """
        if b is None:
            a, b = self._last_pos, a
        if a is None or b is None:
            return None
        def deg2rad(d):
            return d / 180 * math.pi
        # https://en.wikipedia.org/wiki/Haversine_formula
        ph1, ph2 = map(deg2rad, (a[1], b[1]))
        la1, la2 = map(deg2rad, (a[2], b[2]))
        dph = ph2 - ph1
        dla = la2 - la1
        r = 6371
        t = (math.sin(dph/2)**2
             + math.cos(ph1) * math.cos(ph2) * math.sin(dla/2)**2)
        d = 2 * r * math.asin(math.sqrt(t))
        return d
