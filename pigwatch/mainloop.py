import time
import sys

__all__ = [ "Mainloop", "main" ]

class Mainloop:
    def __init__(self):
        self._next_tid = 1
        self._timeouts = []

    def add_timeout(self, interval, cb):
        tid = self._next_tid
        self._next_tid += 1
        self._timeouts.append([
            tid, time.time() + interval, interval, cb])
        return tid

    def source_remove(self, tid):
        for i, to in enumerate(self._timeouts):
            if to[0] == tid:
                del self._timeouts[i]
                return

    def _until_next_timeout(self):
        now = time.time()
        next_time = None
        for to in self._timeouts:
            until = to[1] - now
            if next_time is None or until < next_time:
                next_time = until
                if next_time <= 0:
                    break
        return next_time

    def _run_timeouts(self):
        to_del = []
        now = time.time()
        for i, to in enumerate(self._timeouts):
            if now >= to[0]:
                to[1] += to[2]
                try:
                    ret = to[3]()
                except Exception as e:
                    sys.print_exception(e)
                    print("cb %r threw exception: %r" % (to[3].__name__, e))
                    ret = False
                if not ret:
                    to_del.append(i)
        if to_del:
            to_del.reverse()
            for i in to_del:
                del self._timeouts[i]

    def run(self):
        while True:
            s = self._until_next_timeout()
            if s is None:
                print("mainloop: no further timeouts...")
                return
            if s > 0:
                time.sleep(s) # todo: is there sth more power-efficient?
            self._run_timeouts()


_main = None
def main():
    global _main
    if _main is None:
        _main = Mainloop()
    return _main
