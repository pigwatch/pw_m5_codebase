
from machine import RTC

from m5stack import *

from pigwatch.mainloop import *
from pigwatch.gps_tracker import *
from pigwatch.gps_logfile import *

class GPSRecorder:
    def __init__(self):
        self.rtc = RTC()
        self.gps = GPSTracker(rtc=self.rtc)
        self.gps.register_on_motion(10, self.on_new_pose) # moved more than 10 meters

        self.log = GPSLogfile("gps.log", self.gps, 30)

    def on_new_pose(self):
        self.log.record()
        lcd.clear(lcd.BLACK)
        lcd.text(lcd.CENTER, 0, 'gps rec')
        step = 20
        o = step
        lp = self.gps._last_pos
        if lp:
            lcd.text(lcd.CENTER, o, "%.4f" % lp[1])
            o += step
            lcd.text(lcd.CENTER, o, "%.4f" % lp[2])
            o += step
            lcd.text(lcd.CENTER, o, "alt %.0f" % lp[3])
            o += step
            lcd.text(lcd.CENTER, o, "n %s" % lp[4])
            o += step


def gps_record():
    print("this is pigwatch.tests.gps_record")
    lcd.clear(lcd.BLACK)
    lcd.text(lcd.CENTER, 0, 'gps rec')

    ret = GPSRecorder()
    main().run()
