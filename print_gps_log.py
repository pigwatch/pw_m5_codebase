#!/usr/bin/env python3

import sys
import struct

sformat = "fffB"
data_len = struct.calcsize(sformat)

with open(sys.argv[1], "rb") as fp:
    while True:
        ts = fp.read(19)
        if not ts:
            break
        data = struct.unpack(sformat, fp.read(data_len))
        print("%s: %.8f, %.8f, alt %.0f, n_sat %d" % (
            ts, data[0], data[1], data[2], data[3]))
        print("  https://www.google.de/maps/@%.7f,%.7f,20z" % (
            data[0], data[1]))
